class Texte
{
	constructor( langue='FR' )
	{
		this.modifier_langue( langue )
	}

	modifier_langue( langue )
	{
		this.langue = langue
	}

	get_texte( section ,identifiant ,replace_liste=[] )
	{
		if( TEXTE_LISTE[ this.langue ] == null
			|| TEXTE_LISTE[ this.langue ][ section ] == null
			|| TEXTE_LISTE[ this.langue ][ section ][ identifiant ] == null )
		{
			return `<b class="erreur_couleur">Erreur texte !!! (${this.langue}, ${section}, ${identifiant})</b>`
		}

		let texte = TEXTE_LISTE[ this.langue ][ section ][ identifiant ]

		for( const replace of replace_liste )
		{
			texte = texte.replace( replace[0] ,replace[1] )
		}

		return texte
	}

	erreur( identifiant ,replace_liste=[] )
	{
		return this.get_texte( 'erreur' , identifiant, replace_liste )
	}

	menu( identifiant )
	{
		return this.get_texte( 'menu' , identifiant )
	}

	salle( identifiant ,indentifiant_enfant )
	{
		return this.get_texte( `salle_${identifiant}` , indentifiant_enfant )
	}
}