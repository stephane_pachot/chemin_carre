const TEXTE_LISTE = {
	"FR" :
	{
		"erreur" :
		{
			"HTML_A_1" : "Erreur element html"
			,"HTML_B_1" : "Impossible de trouver {ID}<br>Arrêt du jeu !"
			,"HTML_B_2" : "Impossible de trouver {ID1} l'enfant de {ID2}<br>Arrêt du jeu !"
		}

		,"menu" :
		{
			"bienvenue" : "Bonjour et bienvenue sur<br><br>CHEMIN CARRÉ"
			,"titre" : "CHEMIN CARRÉ"

			//Menu Principal
			,"A_1" : "_ Jouer"
				//Menu Jouer sans sauvegarde
				,"A_1_ALT_1" : "_ Nouvelle Partie"
				,"A_2_ALT_1" : "_ Retour"
				//Menu Jouer avec sauvegarde
				,"A_1_ALT_2" : "_ Continuer"
				,"A_2_ALT_2" : "_ Nouvelle Partie"
				,"A_3_ALT_2" : "_ Charger"
				,"A_4_ALT_2" : "_ Retour"
			,"A_2" : "_ Options"
				//Menu Options
				,"A_1_ALT_3" : "_ Choisir langue"
					//Menu Langue
					,"A_1_ALT_5" : "_ Retour"
					,"A_2_ALT_5" : "_ Français"
					,"A_3_ALT_5" : "_ English"
				,"A_2_ALT_3" : "_ Retour"
			,"A_3" : "_ Aide"
				//Menu Aide
				,"A_1_ALT_4" : "_ Retour"
		}

		,"salle_didacticiel" :
		{
			"A_1" : ""
			,"A_2" : ""
		}

		,"salle_1" :
		{
			"A_1" : ""
			,"A_2" : ""
		}
	}

	,"EN" :
	{
		"erreur" :
		{
			"HTML_A_1" : "Error html element"
			,"HTML_B_1" : "Unable to find {ID}<br>Stop the game !"
			,"HTML_B_2" : "Unable to find {ID1} the child of {ID2}<br>Stop the game !"
		}

		,"menu" :
		{
			"bienvenue" : "Hello and welcome to<br><br>CHEMIN CARRÉ"
			,"titre" : "CHEMIN CARRÉ"

			//Menu Principal
			,"A_1" : "_ Play"
				//Menu Jouer sans sauvegarde
				,"A_1_ALT_1" : "_ New Part"
				,"A_2_ALT_1" : "_ Back"
				//Menu Jouer avec sauvegarde
				,"A_1_ALT_2" : "_ Continue"
				,"A_2_ALT_2" : "_ New Part"
				,"A_3_ALT_2" : "_ Load"
				,"A_4_ALT_2" : "_ Back"
			,"A_2" : "_ Options"
				//Menu Options
				,"A_1_ALT_3" : "_ Choose language"
					//Menu Langue
					,"A_1_ALT_5" : "_ Back"
					,"A_2_ALT_5" : "_ Français"
					,"A_3_ALT_5" : "_ English"
				,"A_2_ALT_3" : "_ Back"
			,"A_3" : "_ Help"
				//Menu Aide
				,"A_1_ALT_4" : "_ Back"
		}

		,"salle_didacticiel" :
		{
			"A_1" : ""
			,"A_2" : ""
		}

		,"salle_1" :
		{
			"A_1" : ""
			,"A_2" : ""
		}
	}
}