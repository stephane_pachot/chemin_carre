class Salle
{
	constructor()
	{
		this.salle_container_objet = document.querySelector( '#salle' )

		if( this.salle_container_objet == null )
		{
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_1' ,[ [ '{ID}' ,'#salle' ] ] ) )
		}
	}

	initialiser_texte( id ,id_texte )
	{
		const salle = this.salle_container_objet.querySelector( `#salle_${id}` )

		if( salle == null )
		{
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,`#salle_${id}` ] ,[ '{ID2}' ,'#salle' ] ] ) )
		}

		const texte_salle = salle.querySelector( `#salle_${id}_texte_${id_texte}` )
		if( texte_salle == null )
		{
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,`#salle_${id}_texte_${id_texte}` ] ,[ '{ID2}' ,`#salle_${id}` ] ] ) )
		}

		texte_salle.innerHTML = TEXTE.salle( id ,id_texte )
	}

	ouvrir( id=false )
	{
		if( id === false )
		{
			this.salle_container_objet.style.display = 'block'
		}
		else
		{
			const salle = this.salle_container_objet.querySelector( `#salle_${id}` )
	
			if( salle == null )
			{
				MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,`#salle_${id}` ] ,[ '{ID2}' ,'#salle' ] ] ) )
			}
	
			salle.style.display = 'block'
		}
	}
}