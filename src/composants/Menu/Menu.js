class Menu
{
	constructor()
	{
		this.menu_object = document.querySelector( '#menu' )

		if( this.menu_object == null )
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_1' ,[ [ '{ID}' ,'#menu' ] ] ) )

	}

	choix( id ,alt=false )
	{
		const choix_menu = this.menu_object.querySelector( `#menu_${id}` )
		if( choix_menu == null )
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,`#menu_${id}` ] ,[ '{ID2}' ,'#menu' ] ] ) )

		
		if( alt !== false )
		{
			id += `_${alt}`
			choix_menu.dataset.alt = alt
		}
		else
		{
			delete choix_menu.dataset.alt
		}

		choix_menu.innerHTML = TEXTE.menu( id )
		choix_menu.classList.remove( 'cacher' )
	}


	bienvenue()
	{
		const choix_menu = this.menu_object.querySelector( '#menu_bienvenue' )

		if( choix_menu == null )
		{
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,'#menu_bienvenue' ] ,[ '{ID2}' ,'#menu' ] ] ) )
		}

		choix_menu.innerHTML = TEXTE.menu( 'bienvenue' )
	}

	titre()
	{
		const choix_menu = this.menu_object.querySelector( '#menu_titre' )

		if( choix_menu == null )
		{
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,'#menu_titre' ] ,[ '{ID2}' ,'#menu' ] ] ) )
		}

		choix_menu.innerHTML = TEXTE.menu( 'titre' )
	}


	click( id ,alt ,fonction )
	{
		const choix_menu = this.menu_object.querySelector( `#menu_${id}` )
		if( choix_menu == null )
			MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,`#menu_${id}` ] ,[ '{ID2}' ,'#menu' ] ] ) )

		if( this.click == null )
			this.click = {}
		if( this.click[ id ] == null )
			this.click[ id ] = {}

		if( alt !== false )
		{
			if( this.click[ id ][ alt ] == null )
				this.click[ id ][ alt ] = []
			this.click[ id ][ alt ] = fonction
		}
		else
		{
			if( this.click[ id ][ 'no_alt' ] == null )
				this.click[ id ][ 'no_alt' ] = []
			this.click[ id ][ 'no_alt' ] = fonction
		}
	}


	charger_les_click()
	{
		for( const id in this.click )
		{
			const choix_menu = this.menu_object.querySelector( `#menu_${id}` )
			if( choix_menu == null )
				MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,`#menu_${id}` ] ,[ '{ID2}' ,'#menu' ] ] ) )

			choix_menu.addEventListener( 'click' ,(elem) => {
				elem = ( elem.target.tagName === 'B' )
					?elem.target.parentElement
					:elem.target
				for( const alt in this.click[id] )
				{
					if(
						elem.dataset.alt == null && alt === 'no_alt'
						|| elem.dataset.alt === alt
					)
					{
						this.click[ id ][ alt ]()
						break
					}
				}
			})
		}
	}


	supprimer()
	{
		for( let id = 1; id <= 9; id++ )
		{
			const choix_menu = this.menu_object.querySelector( `#menu_A_${id}` )
			if( choix_menu == null )
				MESSAGE.erreur( TEXTE.erreur( 'HTML_A_1' ) ,TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,`#menu_A_${id}` ] ,[ '{ID2}' ,'#menu' ] ] ) )

			choix_menu.innerHTML = ''
			choix_menu.classList.add( 'cacher' )
			delete choix_menu.dataset.alt
		}
	}


	ouvrir()
	{
		this.menu_object.style.display = ''
	}

	fermer()
	{
		this.menu_object.classList.remove( 'commencement' )
		this.menu_object.style.display = 'none'
	}

//GÉNÉRER CHOIX MENU Début
	generer_principal = () =>
	{
		this.supprimer()
		this.choix( 'A_1' )
		this.choix( 'A_2' )
		this.choix( 'A_3' )
	}

	generer_jouer_sans_sauvegarde = () =>
	{
		this.supprimer()
		this.choix( 'A_1' ,'ALT_1' )
		this.choix( 'A_2' ,'ALT_1' )
	}

	generer_jouer_avec_sauvegarde = () =>
	{
		this.supprimer()
		this.choix( 'A_1' ,'ALT_2' )
		this.choix( 'A_2' ,'ALT_2' )
		this.choix( 'A_3' ,'ALT_2' )
		this.choix( 'A_4' ,'ALT_2' )
	}

	generer_option = () =>
	{
		this.supprimer()
		this.choix( 'A_1' ,'ALT_3' )
		this.choix( 'A_2' ,'ALT_3' )
	}

	generer_langue = () =>
	{
		this.supprimer()
		this.choix( 'A_1' ,'ALT_5' )
		this.choix( 'A_2' ,'ALT_5' )
		this.choix( 'A_3' ,'ALT_5' )
	}

	generer_aide = () =>
	{
		this.supprimer()
		this.choix( 'A_1' ,'ALT_4' )
	}
//GÉNÉRER CHOIX MENU Fin
}