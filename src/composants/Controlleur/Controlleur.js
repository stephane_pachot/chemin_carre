class Controlleur
{
	constructor()
	{
	}

	generer_premier_menu()
	{
		MENU.bienvenue()
		MENU.titre()

		MENU.generer_principal()

		//CHOIX Jouer
		if( false )//Jouer si sauvegarde existe ou non
		{
			//Jouer (avec sauvegarde)
			MENU.click( 'A_1' ,false ,MENU.generer_jouer_avec_sauvegarde )
				//MENU.click( 'A_1' ,'ALT_2' ,MENU.generer_ )
				//MENU.click( 'A_2' ,'ALT_2' ,MENU.generer_ )
				//MENU.click( 'A_3' ,'ALT_2' ,MENU.generer_ )
				//Jouer avec sauvegarde -> Retour
				MENU.click( 'A_4' ,'ALT_2' ,MENU.generer_principal )
		}
		else
		{
			//Jouer (sans sauvegarde)
			MENU.click( 'A_1' ,false ,MENU.generer_jouer_sans_sauvegarde )
				//MENU.click( 'A_1' ,'ALT_1' ,MENU.generer_ )
				//Jouer sans sauvegarde -> Retour
				MENU.click( 'A_2' ,'ALT_1' ,MENU.generer_principal )
		}

		
		//CHOIX Options
		MENU.click( 'A_2' ,false ,MENU.generer_option )
			//Options -> Choisir langue
			MENU.click( 'A_1' ,'ALT_3' ,MENU.generer_langue )
				//Options -> Choisir langue -> Retour
				MENU.click( 'A_1' ,'ALT_5' ,MENU.generer_option )
				//Options -> Choisir langue -> Français
				MENU.click( 'A_2' ,'ALT_5' ,() => { TEXTE.modifier_langue( 'FR' );MENU.generer_option() } )
				//Options -> Choisir langue -> English
				MENU.click( 'A_3' ,'ALT_5' ,() => { TEXTE.modifier_langue( 'EN' );MENU.generer_option() } )
			//Options -> Retour
			MENU.click( 'A_2' ,'ALT_3' ,MENU.generer_principal )

		//CHOIX Aide
		MENU.click( 'A_3' ,false ,MENU.generer_aide )
			//Aide -> Retour
			MENU.click( 'A_1' ,'ALT_4' ,MENU.generer_principal )

		MENU.charger_les_click()
	}
}