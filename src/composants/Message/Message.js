class Message
{
	constructor()
	{
		this.message_objet = document.querySelector( '#message' )
		if ( this.message_objet == null )
		{
			throw new Error( `${ TEXTE.erreur('HTML_A_1') } : ${ TEXTE.erreur( 'HTML_B_1' ,[ [ '{ID}' ,'#message' ] ] ) }` )
		}

		this.message_titre_objet = this.message_objet.querySelector( '#message_titre' )
		if ( this.message_titre_objet == null )
		{
			throw new Error( `${ TEXTE.erreur('HTML_A_1') } : ${ TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,'#message_titre' ] ,[ '{ID2}' ,'#message' ] ] ) }` )
		}

		this.message_contenu_objet = this.message_objet.querySelector( '#message_contenu' )
		if ( this.message_contenu_objet == null )
		{
			throw new Error( `${ TEXTE.erreur('HTML_A_1') } : ${ TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,'#message_contenu' ] ,[ '{ID2}' ,'#message' ] ] ) }` )
		}

		const message_objet = document.querySelector( '#message_fermer' )
		if ( message_objet == null )
		{
			throw new Error( `${ TEXTE.erreur('HTML_A_1') } : ${ TEXTE.erreur( 'HTML_B_2' ,[ [ '{ID1}' ,'#message_contenu' ] ,[ '{ID2}' ,'#message' ] ] ) }` )
		}
		message_objet.addEventListener( 'click' ,this.cacher )
	}

	afficher( titre=false ,message=false )
	{
		if( titre !== false )
		{
			this.message_titre_objet.innerHTML = titre
		}
		if( message !== false )
		{
			this.message_contenu_objet.innerHTML = message
		}

		if( this.message_objet.style.display != 'block' )
		{
			this.message_objet.style.display = 'block'
		}

		return `${titre} : ${message}`
	}

	erreur( titre=false ,message=false )
	{
		this.afficher( titre ,message )
		throw new Error( `${titre} : ${message}` )
	}

	cacher()
	{
		if( this.message_objet.style.display != '' )
		{
			this.message_objet.style.display = ''
		}
	}
}