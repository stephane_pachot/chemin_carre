var MENU, TEXTE, MESSAGE, SALLE

const main = () =>
{
	TEXTE = new Texte()//Permet d'afficher les différents textes
	MESSAGE = new Message()//Permet d'afficher des messages sous forme d'alerte
	SALLE = new Salle()//Permet de gérer les différentes salles
	MENU = new Menu()//Le menu de navigation, affiche la bienvenue, le titre, permet de commencer le jeu, ou d'aller dans les options, etc.

	const CONTROLLEUR = new Controlleur()

	CONTROLLEUR.generer_premier_menu()
}

window.addEventListener( 'DOMContentLoaded' ,main )